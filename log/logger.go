package log

/**
  @Author   : bob
  @Datetime : 2023-08-12 上午 6:41
  @File     : logger.go
  @Desc     :
*/

import (
	"fmt"
	rotate "github.com/lestrrat-go/file-rotatelogs"
	"github.com/sirupsen/logrus"
	"io"
	"os"
	"path/filepath"
	"time"
)

var log = logrus.New()

func init() {
	path := "./log"
	writer, err := rotate.New(
		filepath.Join(path, fmt.Sprintf("backend-go-%s.log", time.Now().Format("20060102"))),
		rotate.WithLinkName(filepath.Join(path, "backend-go.log")),
		rotate.WithRotationCount(7),
		rotate.WithRotationTime(time.Minute*1),
	)
	if err == nil {
		log.SetOutput(io.MultiWriter(writer, os.Stdout))
	} else {
		log.Fatal("Config logger failure, ", err)
	}

	log.SetFormatter(&logrus.TextFormatter{
		TimestampFormat:           "2006-01-02 15:04:05",
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		FullTimestamp:             true,
		DisableLevelTruncation:    true,
	})
	log.SetLevel(logrus.InfoLevel)
}

func New() *logrus.Logger {

	return log
}
