package main

import (
	"backend-go/log"
	"backend-go/router"
)

/**
  @Author   : bob
  @Datetime : 2023-08-12 上午 6:33
  @File     : main.go
  @Desc     :
*/

var logger = log.New()

func startServer() {
	router := router.SetupRouter()
	err := router.Run(":9999")
	if err != nil {
		logger.Errorf("The App stats failure, %s", err)
	} else {
		logger.Info("The App stats success")
	}
}

func main() {
	startServer()
}


