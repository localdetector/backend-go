VERSION=1.0.0
CURRENT_TIME=$(shell date +"%Y-%m-%d %H:%M:%S")


all: clean build

build:
	mkdir build
	GOOS=linux GOARCH=amd64 go build \
		-ldflags="-s -w -X 'cmd.buildVersion=$(VERSION)' -X 'cmd.buildTime=$(CURRENT_TIME)'" \
		-x -v -o build/backend-go

clean:
	rm -rf build

push:
	git pull
	git add .
	git commit -m "update" || true
	git push origin main || true
