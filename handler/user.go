package handler

import (
	"backend-go/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

/**
  @Author   : bob
  @Datetime : 2023-08-12 上午 6:35
  @File     : user.go
  @Desc     :
*/

var logger = log.New()

func UserSave(context *gin.Context) {
	name := context.Param("name")
	logger.Infof("Save user to db, user is %s", name)
	context.JSON(http.StatusOK, gin.H{
		"name": name,
		"msg":  "welcome to the city",
	})
}

func QueryUserList(context *gin.Context) {
	nameList := [3]string{
		"bob",
		"Gloria",
		"BJ",
	}
	logger.Info("Query user success")
	context.JSON(http.StatusOK, nameList)
}

func Hello(context *gin.Context) {
	logger.Info("hello, this is backend-go")
	context.JSON(http.StatusOK, gin.H{
		"msg": "hello world",
	})
}
