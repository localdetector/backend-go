package router

import (
	"backend-go/handler"
	"github.com/gin-gonic/gin"
)

/**
  @Author   : bob
  @Datetime : 2023-08-12 上午 7:25
  @File     : router.go
  @Desc     :
*/

func SetupRouter() *gin.Engine {
	router := gin.Default()
	router.StaticFile("./datou.jpg", "./datou.jpg")
	router.GET("hello", handler.Hello)
	userRouter := router.Group("/user")
	{
		userRouter.GET("/save/:name", handler.UserSave)
		userRouter.GET("/list", handler.QueryUserList)
	}
	return router

}
