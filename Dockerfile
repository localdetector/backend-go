FROM alpine:3.18.3

WORKDIR data
COPY backend-go-linux .

USER nobody

ENTRYPOINT ["./backend-go-linux"]
